# proxydetect

This library is motivated by [Daniel Stenberg's post](https://daniel.haxx.se/blog/2022/08/12/the-dream-of-auto-detecting-proxies/) about why curl doesn't use libproxy to automatically detect the user's proxy. I figure, may as well take a shot at providing a library which fulfill's curl's needs for a proxy library. So with that in mind, the goals of this library:

* Take inspiration from the strengths of libproxy - provide simple usage options so that people who just want the defaults can use them
* Provide good documentation on how the library works
* Ensure good test coverage
* Add the ability to go beyond the defaults, by specifying things like URL parsing/retrieval external to the library
* Add a non-blocking option to use the library, for apps (like curl) which want to be able to block as little as humanly possible

It's my hope that this will be useful to someone, or if not, at least hopefully nobody will think I'm full of hubris for trying. :)
