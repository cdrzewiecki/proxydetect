default_target := "dev"
default: fmt build

fmt:
  cargo +nightly fmt

build target=default_target: fmt
  cargo build --profile {{target}}
